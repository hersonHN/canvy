(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var Line = require('./scripts/classes/line');
var Point = require('./scripts/classes/point');
var Cell = require('./scripts/classes/cell');
var Board = require('./scripts/classes/board');

Board.Line = Line;
Board.Point = Point;
Board.Cell = Cell;
Board.Board = Board;

module.exports = Board;

},{"./scripts/classes/board":2,"./scripts/classes/cell":3,"./scripts/classes/line":4,"./scripts/classes/point":5}],2:[function(require,module,exports){
'use strict';

var Cell = require('./cell');
var Point = require('./point');
var Line = require('./line');

function Board(conf) {
    this.cells = {};
    this.points = {};
    this.lines = {};

    this.cellList = [];
    this.pointList = [];
    this.lineList = [];

    if (conf) this.init(conf);
}


// abstract functions
Board.prototype.beforeSelectCell = function (cell, board) {};
Board.prototype.afterSelectCell  = function (cell, board) {};
Board.prototype.beforeDraw = function (board) {};
Board.prototype.afterDraw  = function (board) {};
Board.prototype.onDrawCell = function (cell, board, ctx) {};


Board.prototype.init = function (conf) {
    var xInterval = Math.sin(Math.PI / 6);
    var yInterval = Math.cos(Math.PI / 6);

    var xDistance = conf.distance * xInterval;
    var yDistance = conf.distance * yInterval;

    if (!conf.horizontalOffset) conf.horizontalOffset = 0;
    if (!conf.verticalOffset) conf.verticalOffset = 0;

    this.conf = conf;

    this.generateCells(xInterval, yInterval);
    this.generateCellBoundaries();
    this.generatePoints(xInterval, yInterval);
    this.linkPoints(xInterval, yInterval);
    this.generateLines();
    this.calculateBoundaries(this.conf);
};


Board.prototype.draw = function (ctx, opt) {
    opt = opt || {};

    this.beforeDraw(this);
    this.drawLines(ctx);
    this.drawCells(ctx, opt);

    if (opt.debug && opt.drawPoints) {
        this.drawPoints(ctx);
    }

    this.drawHilight(ctx);

    this.afterDraw(this);
};


Board.prototype.generateCells = function (xInterval, yInterval) {
    var cols, rows, cell, c, r, isEven;

    cols = this.conf.columns;

    for (c = 1; c <= cols; c++) {

        isEven = (c % 2 === 0);
        rows = isEven ? this.conf.evenRows : this.conf.evenRows - 1;

        for (r = 1; r <= rows; r++) {

            cell = new Cell({
                row: r,
                column: c,
                isEven: isEven,
                x: c * (xInterval + 1) * 0.5 - xInterval * 0.5,
                y: (r - (isEven ? 0.5 : 0)) * yInterval
            });

            this.cells[cell.name] = cell;
            this.cellList.push(cell);
        }
    }
};


Board.prototype.generatePoints = function (xInterval, yInterval) {
    var cell, pointL, pointR;

    this.cellList.forEach(function (cell) {
        var name, nameL, nameR;

        name = cell.name;
        nameL = cell.name + 'L';
        nameR = cell.name + 'R';

        pointL = new Point({
            x: cell.x - xInterval * 0.5,
            y: cell.y - yInterval * 0.5,
            name: nameL
        });

        pointR = new Point({
            x: cell.x + xInterval * 0.5,
            y: cell.y - yInterval * 0.5,
            name: nameR
        });

        cell.points.push(pointR);
        cell.points.push(pointL);

        this.points[nameL] = pointL;
        this.points[nameR] = pointR;

        this.pointList.push(pointL);
        this.pointList.push(pointR);
    }, this);
};


Board.prototype.generateCellBoundaries = function () {
    var board = this;

    this.cellList.forEach(function (cell) {
        var nn, ss, nw, ne, sw, se;

        nn = Cell.getName(cell.row - 1, cell.column);
        ss = Cell.getName(cell.row + 1, cell.column);

        if (cell.isEven) {
            nw = Cell.getName(cell.row - 1, cell.column - 1);
            ne = Cell.getName(cell.row - 1, cell.column + 1);
            sw = Cell.getName(cell.row,     cell.column - 1);
            se = Cell.getName(cell.row,     cell.column + 1);
        } else {
            nw = Cell.getName(cell.row,     cell.column - 1);
            ne = Cell.getName(cell.row,     cell.column + 1);
            sw = Cell.getName(cell.row + 1, cell.column - 1);
            se = Cell.getName(cell.row + 1, cell.column + 1);
        }

        if (board.cells[nn]) cell.boundaries.push(board.cells[nn]);
        if (board.cells[ss]) cell.boundaries.push(board.cells[ss]);
        if (board.cells[nw]) cell.boundaries.push(board.cells[nw]);
        if (board.cells[ne]) cell.boundaries.push(board.cells[ne]);
        if (board.cells[sw]) cell.boundaries.push(board.cells[sw]);
        if (board.cells[se]) cell.boundaries.push(board.cells[se]);

    });
};


Board.prototype.linkPoints = function (xInterval, yInterval) {

    this.cellList.forEach(function (cell) {
        var bottomCellName, leftCellName, rightCellName;
        var wPoint, ePoint, swPoint, sePoint;

        bottomCellName = Cell.getName(cell.row + 1, cell.column);
        leftCellName   = Cell.getName(cell.isEven ? cell.row : cell.row + 1, cell.column - 1);
        rightCellName  = Cell.getName(cell.isEven ? cell.row : cell.row + 1, cell.column + 1);

        swPoint = bottomCellName + 'L';
        sePoint = bottomCellName + 'R';
        wPoint  = leftCellName   + 'R';
        ePoint  = rightCellName  + 'L';

        // link the point on counter clock order
        // starting from west to east
        this.linkPoint(cell, { name: wPoint,  x: cell.x - 0.5, y: cell.y });
        this.linkPoint(cell, { name: swPoint, x: cell.x - xInterval * 0.5, y: cell.y + yInterval * 0.5 });
        this.linkPoint(cell, { name: sePoint, x: cell.x + xInterval * 0.5, y: cell.y + yInterval * 0.5 });
        this.linkPoint(cell, { name: ePoint,  x: cell.x + 0.5, y: cell.y });
    }, this);
};


Board.prototype.linkPoint = function (cell, o) {
    var name = o.name;
    var point;

    if (this.points.hasOwnProperty(name)) {
        point = this.points[name];
        cell.points.push(point);
    } else {
        point = new Point(o);
        this.points[name] = point;
        this.pointList.push(point);
        cell.points.push(point);
    }
};


Board.prototype.generateLines = function () {
    var board = this;

    board.cellList.forEach(function (cell) {
        var firstPoint = cell.points[0];

        var lastPoint = cell.points.reduce(function (pointA, pointB, index) {
            board.generateLine(cell, pointA, pointB);
            return pointB;
        });

        board.generateLine(cell, lastPoint, firstPoint);
    });
};


Board.prototype.calculateBoundaries = function () {
    this.cellList.forEach(function (cell) {
        cell.calculateBoundaries();
    });
};


Board.prototype.generateLine = function (cell, pointA, pointB) {
    // build the line name
    var lineName, line;

    lineName = Line.getName(pointA.name, pointB.name);

    if (this.lines.hasOwnProperty(lineName)) {
        line = this.lines[lineName];
        cell.lines.push(line);
        return;
    }

    line = new Line({
        pointA: pointA,
        pointB: pointB,
        name: lineName
    });

    this.lines[lineName] = line;
    this.lineList.push(line);
    cell.lines.push(line);
};


Board.prototype.drawLines = function (ctx) {
    var board = this;

    ctx.strokeStyle = board.conf.lineColor;

    board.lineList.forEach(function (line) {
        board.onDrawLine(line, board, ctx);
    });
};


Board.prototype.onDrawLine = function (line, board, ctx) {
    var distance = this.conf.distance;
    var horizontalOffset = this.conf.horizontalOffset;
    var verticalOffset   = this.conf.verticalOffset;

    var aX = line.pointA.x * distance + horizontalOffset;
    var aY = line.pointA.y * distance + verticalOffset;
    var bX = line.pointB.x * distance + horizontalOffset;
    var bY = line.pointB.y * distance + verticalOffset;

    ctx.beginPath();
    ctx.moveTo(aX, aY);
    ctx.lineTo(bX, bY);
    ctx.stroke();
};


Board.prototype.drawCells = function (ctx, opt) {
    var board = this;
    this.cellList.forEach(function (cell) {
        board.onDrawCell(cell, board, ctx);

        if (opt.debug == true && opt.drawCenter == true) {
            board.drawPoint(ctx, cell);
        }
    });
};


Board.prototype.drawPoints = function (ctx) {
    var xDistance, yDistance;
    var board = this;

    var distance = this.conf.distance;
    xDistance = distance * Math.sin(Math.PI / 6);
    yDistance = distance * Math.cos(Math.PI / 6);

    board.pointList.forEach(function (point) {
        board.drawPoint(ctx, point);
    });
};


Board.prototype.drawPoint = function (ctx, point, o) {
    o = o || {};
    o.horizontalOffset = o.horizontalOffset || this.conf.horizontalOffset;
    o.verticalOffset   = o.verticalOffset   || this.conf.verticalOffset;

    var x = point.x * this.conf.distance + o.horizontalOffset;
    var y = point.y * this.conf.distance + o.verticalOffset;
    ctx.fillStyle = point.color || 'black';

    ctx.font = '10px monospace';
    ctx.fillText(point.name, x + 10, y + 10);

    ctx.beginPath();
    ctx.arc(x, y, 5, 0, Math.PI * 2, true);
    ctx.fill();
};

Board.prototype.drawHilight = function (ctx) {
    var hovered = this.hoveredCell;

    if (!hovered) return;
    var conf = this.conf;
    var board = this;

    ctx.strokeStyle = conf.hoverColor;

    hovered.lines.forEach(function (line) {
        board.onDrawLine(line, board, ctx);
    });

    ctx.strokeStyle = conf.lineColor;
};


Board.prototype.selectCellFromPoint = function (p, callback) {
    var board = this;
    var x = (p.x - board.conf.horizontalOffset) / board.conf.distance;
    var y = (p.y - board.conf.verticalOffset)   / board.conf.distance;


    var cell, isWithinBoundaries, isPointInside;

    for (var i = board.cellList.length - 1; i >= 0; i--) {

        cell = board.cellList[i];

        isWithinBoundaries = ((cell.minX <= x && cell.maxX >= x) && (cell.minY <= y && cell.maxY >= y));
        if (!isWithinBoundaries) continue;

        isPointInside = cell.isPointInside({ x: x, y: y });
        if (!isPointInside) continue;

        if (typeof callback == 'function') {
            callback(cell, board);
            break;
        }

        return cell;
    }
};



Board.prototype.selectCell = function (cell, board) {
    cell.selected = !cell.selected;
};


Board.prototype.hoverCell = function (cell, board) {
    board.hoveredCell = cell;
};



Board.prototype.savePoints = function () {
    this.pointList.forEach(savePoints);
    this.cellList.forEach(savePoints);
};
function savePoints(point) {
    var saved = {
        x: point.x || 0,
        y: point.y || 0,
        z: point.z || 0
    }
    point.saved = saved;
}


Board.prototype.loadPoints = function () {
    this.pointList.forEach(loadPoints);
    this.cellList.forEach(loadPoints);
};
function loadPoints(point) {
    if (!point.saved) return;

    point.x = point.saved.x;
    point.y = point.saved.y;
    point.z = point.saved.z;
}

module.exports = Board;

},{"./cell":3,"./line":4,"./point":5}],3:[function(require,module,exports){
'use strict';

function Cell (p) {
    this.row = p.row;
    this.column = p.column;
    this.x = p.x;
    this.y = p.y;
    this.isEven = p.isEven;
    this.name = Cell.getName(this.row, this.column);

    this.points = [];
    this.lines = [];
    this.boundaries = [];

    this.maxX = 0;
    this.maxY = 0;
    this.minX = 0;
    this.minY = 0;
}

// static functions
Cell.getName = function (r, c) {
    return 'r' + r + 'c' + c;
};

// instance functions
Cell.prototype.calculateBoundaries = function () {
    var p, point;
    var maxX, minX, maxY, minY;

    this.points.forEach(function (point) {
        maxX = max(point.x, maxX);
        maxY = max(point.y, maxY);
        minX = min(point.x, minX);
        minY = min(point.y, minY);
    });

    this.maxX = maxX;
    this.maxY = maxY;
    this.minX = minX;
    this.minY = minY;

    this.calculateCenter();
};

Cell.prototype.calculateCenter = function () {
    var pointA = this.points[0];
    var pointB = this.points[3];

    this.x = (pointA.x + pointB.x) / 2;
    this.y = (pointA.y + pointB.y) / 2;
}

Cell.prototype.isPointInside = function (point) {
    var line, response, pointA, pointB;

    for (var i = 0; i < this.lines.length; i++) {
        line = this.lines[i];
        response = isPointInsideTriangle(point, line.pointA, line.pointB, this);
        if (response) {
            return true;
        }
    }
    return false;
};

function isPointInsideTriangle(p, p1, p2, p3) {
    // using barycentric coordinates
    var alpha = ((p2.y - p3.y) * (p.x  - p3.x) + (p3.x - p2.x) * (p.y  - p3.y)) /
                ((p2.y - p3.y) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.y - p3.y));
    var beta  = ((p3.y - p1.y) * (p.x  - p3.x) + (p1.x - p3.x) * (p.y  - p3.y)) /
                ((p2.y - p3.y) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.y - p3.y));
    var gamma = 1.0 - alpha - beta;

    return (alpha > 0 && beta > 0 && gamma > 0);
}

function max(x, y) {
    if (isNaN(x)) return y;
    if (isNaN(y)) return x;
    return Math.max(x, y);
}

function min(x, y) {
    if (isNaN(x)) return y;
    if (isNaN(y)) return x;
    return Math.min(x, y);
}

module.exports = Cell;

},{}],4:[function(require,module,exports){
'use strict';

function Line (p) {
    this.name = p.name;
    this.pointA = p.pointA;
    this.pointB = p.pointB;
}

Line.getName = function (pointA, pointB) {
    if (pointA > pointB) {
        return pointA + '-' + pointB;
    } else {
        return pointB + '-' + pointA;
    }
}

Line.prototype.toString = function () {
    return this.name;
}

module.exports = Line;

},{}],5:[function(require,module,exports){
'use strict';

function Point (p) {
    this.x = p.x;
    this.y = p.y;
    this.z = 0;
    this.name = p.name;
}

Point.prototype.toString = function () {
    return this.name;
}

module.exports = Point;

},{}],6:[function(require,module,exports){
'use strict';

var animator = {};

animator.queu = [];
animator.lastTimeStamp;

animator.move = function (p) {
    var element = p.move;
    var start = p.from;
    var end = p.to;
    var seconds = p.time;

    if (element.constructor.name == 'Cell') {
        animator.queu.push({
            objectToMove: element,
            startPoint: { x: start.x, y: start.y },
            endPoint: end,
            currentTime: 0,
            totalTime: seconds
        });

        element.points.forEach(function (point, n) {
            animator.queu.push({
                objectToMove: point,
                startPoint: { x: point.x, y: point.y },
                endPoint: end.points[n],
                currentTime: 0,
                totalTime: seconds
            });
        });
    }
};

animator.forward = function (timestamp) {
    if (!animator.lastTimeStamp) {
        animator.lastTimeStamp = timestamp;
        return;
    }

    if (timestamp < animator.lastTimeStamp) {
        animator.lastTimeStamp = timestamp;
        return;
    }

    var delta = timestamp - animator.lastTimeStamp;

    animator.lastTimeStamp = timestamp;
    animator.processQueue(delta / 1000);
};

animator.processQueue = function (delta) {

    animator.queu = animator.queu.map(function (animation) {
        var objectToMove = animation.objectToMove;
        var startPoint = animation.startPoint;
        var endPoint = animation.endPoint;
        var currentTime = animation.currentTime;
        var totalTime = animation.totalTime;

        animation.currentTime += delta;

        var distanceX = endPoint.x - startPoint.x;
        var distanceY = endPoint.y - startPoint.y;

        var movementPercentage = currentTime / animation.totalTime;

        var targetX = startPoint.x + distanceX * movementPercentage;
        var targetY = startPoint.y + distanceY * movementPercentage;

        if (animation.currentTime < animation.totalTime) {
            animation.objectToMove.x = targetX;
            animation.objectToMove.y = targetY;
        } else {
            animation.objectToMove.x = endPoint.x;
            animation.objectToMove.y = endPoint.y;
        }

        return animation;
    });

    animator.queu = animator.queu.filter(function (animation) {
        return (animation.currentTime < animation.totalTime);
    });
}

module.exports = animator;

},{}],7:[function(require,module,exports){
'use strict';

var canvas = document.getElementById('canvas');

module.exports = {
    canvas: canvas,
    context: canvas.getContext('2d')
};

},{}],8:[function(require,module,exports){
'use strict';

var conf = {
    distance: 120,
    columns: 10,
    evenRows: 10,
    hilightColor: 'rgba(136, 221, 199, 0.5)',
    lineColor: 'rgba(0, 0, 0, 0.4)',
    hoverColor: 'rgb(0, 0, 0)'
};

module.exports = conf;

},{}],9:[function(require,module,exports){
'use strict';

// Submodules
var Board = require('hexboard');
var canv = require('./canvas');
var configuration = require('./configuration');
var transformations = require('./transformations');
var animator = require('./animator');
var hoverCell = require('./hover-cell')


// local variables
var canvas = canv.canvas, context = canv.context;
var board, conf;


conf = Object.create(configuration);
conf.horizontalOffset = 100;
conf.verticalOffset   = 100;

init();


function init() {
    board = new Board(conf);
    board.savePoints();
    // rotateBoard();

    bindBoardEvents();
    window.addEventListener('resize', canvasResize);
    canvas.addEventListener('click', canvasClick);
    canvas.addEventListener('mousemove', canvasHover);

    canvasResize();

    window.requestAnimationFrame(draw);
}


function rotateBoard() {
    var angle = 2;
    var limits = transformations.getLimits(board);
    var center = limits.center;

    function rotate() {
        board.loadPoints();
        transformations.rotate2d(board, transformations.degreesToRadians(angle), center);
        transformations.rotate2d(board, transformations.degreesToRadians(70), center, ['z', 'y']);
        transformations.perspective(board, limits);
        board.calculateBoundaries();
        angle += 0.5;
    }

    rotate();
    setInterval(rotate, 20);
}

var tm = 0;

function draw(timestamp) {
    clean();
    animator.forward(timestamp);
    board.draw(context, { debug: false, drawCenter: true });
    window.requestAnimationFrame(draw);
}

function clean() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function bindBoardEvents() {
    board.onDrawCell = onDrawCell;
}


function onDrawCell(cell, board, context) {
    var point, p, first;

    if (!cell.selected) return;

    var distance = board.conf.distance;

    context.fillStyle = board.conf.hilightColor;
    context.beginPath();

    for (p in cell.points) {
        point = cell.points[p];

        if (!first) {
            first = point;
            context.moveTo(
                point.x * distance + board.conf.horizontalOffset,
                point.y * distance + board.conf.verticalOffset
            );
        } else {
            context.lineTo(
                point.x * distance + board.conf.horizontalOffset,
                point.y * distance + board.conf.verticalOffset
            );
        }
    }

    context.lineTo(
        first.x * distance + board.conf.horizontalOffset,
        first.y * distance + board.conf.verticalOffset
    );

    context.fill();
}


function canvasResize() {
    var width = window.innerWidth;
    var height = window.innerHeight;

    canvas.width = width;
    canvas.height = height;
}

function canvasClick(event) {
    board.selectCellFromPoint({ x: event.offsetX, y: event.offsetY }, board.selectCell);
}


function canvasHover(event) {
    // select the cell at point
    var cell = board.selectCellFromPoint({ x: event.offsetX, y: event.offsetY });
    if (!cell) return;

    // if it's the already selected cell return
    if (board.hoveredCell && board.hoveredCell.hoverOver == cell.name) return;

    if (!board.hoveredCell) {
        board.hoveredCell = hoverCell.create(cell);
        return;
    }

    board.hoveredCell.hoverOver = cell.name;

    animator.move({
        move: board.hoveredCell,
        from: board.hoveredCell,
        to: cell,
        time: 0.13
    });

}

function zoomCanvas() {
    console.log('zooming');
}

},{"./animator":6,"./canvas":7,"./configuration":8,"./hover-cell":11,"./transformations":12,"hexboard":1}],10:[function(require,module,exports){
'use strict';

var formulas = {};

formulas.polarCoordinates = function (x, y) {
    var ratio, angle;
    ratio = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

    if (x !== 0) {
        angle = Math.atan(y / x);

        if (y >= 0 && x <= 0) {
            angle = Math.PI + angle;
        } else

        if (y < 0 && x < 0) {
            angle = Math.PI + angle;
        } else

        if (y > 0 && x > 0) {
            angle = Math.PI * 2 + angle;
        }
    } else {
        if (y > 0) angle = Math.PI * 0.5;
        if (y < 0) angle = Math.PI * 1.5;
        if (y === 0) angle = 0;
    }

    return { angle: angle, ratio: ratio };
};

formulas.cartesianCoordinates = function (angle, ratio) {
    var x = ratio * Math.cos(angle);
    var y = ratio * Math.sin(angle);

    return { x: x, y: y };
};

module.exports = formulas;

},{}],11:[function(require,module,exports){
'use strict';

var Board = require('hexboard');
var Cell = Board.Cell;
var Point = Board.Point;
var Line = Board.Line;

var hoverCell = {};

hoverCell.create = function (cell) {
    var points = {};
    var hover = new Cell(cell);

    hover.name = 'hover';
    hover.hoverOver = cell.name;

    cell.points.forEach(function (p, n) {
        var point = new Point(p);
        points[p.name] = point;
        point.name = 'hoverpoint' + n;

        hover.points.push(point);
    });

    cell.lines.forEach(function (l, n) {
        var line = new Line(l);
        line.pointA = points[l.pointA.name];
        line.pointB = points[l.pointB.name];

        hover.lines.push(line);
    });

    return hover;
};

module.exports = hoverCell;

},{"hexboard":1}],12:[function(require,module,exports){
'use strict';

var formulas = require('./formulas');
var app = {};

app.rotate2d = function (board, angle, center, coordinates) {
    center = center || app.getLimits(board).center;
    coordinates = coordinates || ['x', 'y'];

    var x = coordinates.shift();
    var y = coordinates.shift();

    board.pointList.forEach(rotatePoint);

    function rotatePoint(point) {
        var polar = formulas.polarCoordinates(point[x] - center[x], point[y] - center[y]);

        polar.angle += angle;

        var cartesian = formulas.cartesianCoordinates(polar.angle, polar.ratio);
        point[x] = cartesian.x + center[x];
        point[y] = cartesian.y + center[y];
    }
};


app.perspective = function (board, originalLimits, eye) {
    eye = eye || {};
    eye.x = eye.x || originalLimits.width / 2;
    eye.y = eye.y || originalLimits.height / 2;
    eye.z = eye.z || 0;

    board.pointList.forEach(function (point) {
        var pointZ = point.z + originalLimits.height;
        var F =  pointZ - eye.z;
        point.x = ((point.x - eye.x) * (F / pointZ)) + eye.x;
        point.y = ((point.y - eye.y) * (F / pointZ)) + eye.y;
    });
};


app.getLimits = function (board) {
    var north, south, east, west;
    var point = board.pointList[0];

    north = point.y;
    south = point.y;
    east = point.x;
    west = point.x;

    board.pointList.forEach(function (point) {
        north = Math.min(north, point.y);
        south = Math.max(south, point.y);
        west  = Math.min(west,  point.x);
        east  = Math.max(east,  point.x);
    });

    return {
        north: north,
        east: east,
        south: south,
        west: west,

        width:  east  - west,
        height: south - north,

        center: {
            x: (east + west) / 2,
            y: (north + south) / 2,
            z: 0
        }
    };
};


app.degreesToRadians = function (deg) {
    return deg / 180 * Math.PI;
};

module.exports = app;

},{"./formulas":10}]},{},[9])