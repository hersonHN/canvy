'use strict';

var conf = {
    distance: 120,
    columns: 10,
    evenRows: 10,
    hilightColor: 'rgba(136, 221, 199, 0.5)',
    lineColor: 'rgba(0, 0, 0, 0.4)',
    hoverColor: 'rgb(0, 0, 0)'
};

module.exports = conf;
