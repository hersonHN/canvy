'use strict';

var formulas = require('./formulas');
var app = {};

app.rotate2d = function (board, angle, center, coordinates) {
    center = center || app.getLimits(board).center;
    coordinates = coordinates || ['x', 'y'];

    var x = coordinates.shift();
    var y = coordinates.shift();

    board.pointList.forEach(rotatePoint);

    function rotatePoint(point) {
        var polar = formulas.polarCoordinates(point[x] - center[x], point[y] - center[y]);

        polar.angle += angle;

        var cartesian = formulas.cartesianCoordinates(polar.angle, polar.ratio);
        point[x] = cartesian.x + center[x];
        point[y] = cartesian.y + center[y];
    }
};


app.perspective = function (board, originalLimits, eye) {
    eye = eye || {};
    eye.x = eye.x || originalLimits.width / 2;
    eye.y = eye.y || originalLimits.height / 2;
    eye.z = eye.z || 0;

    board.pointList.forEach(function (point) {
        var pointZ = point.z + originalLimits.height;
        var F =  pointZ - eye.z;
        point.x = ((point.x - eye.x) * (F / pointZ)) + eye.x;
        point.y = ((point.y - eye.y) * (F / pointZ)) + eye.y;
    });
};


app.getLimits = function (board) {
    var north, south, east, west;
    var point = board.pointList[0];

    north = point.y;
    south = point.y;
    east = point.x;
    west = point.x;

    board.pointList.forEach(function (point) {
        north = Math.min(north, point.y);
        south = Math.max(south, point.y);
        west  = Math.min(west,  point.x);
        east  = Math.max(east,  point.x);
    });

    return {
        north: north,
        east: east,
        south: south,
        west: west,

        width:  east  - west,
        height: south - north,

        center: {
            x: (east + west) / 2,
            y: (north + south) / 2,
            z: 0
        }
    };
};


app.degreesToRadians = function (deg) {
    return deg / 180 * Math.PI;
};

module.exports = app;
