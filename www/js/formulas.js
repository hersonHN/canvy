'use strict';

var formulas = {};

formulas.polarCoordinates = function (x, y) {
    var ratio, angle;
    ratio = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

    if (x !== 0) {
        angle = Math.atan(y / x);

        if (y >= 0 && x <= 0) {
            angle = Math.PI + angle;
        } else

        if (y < 0 && x < 0) {
            angle = Math.PI + angle;
        } else

        if (y > 0 && x > 0) {
            angle = Math.PI * 2 + angle;
        }
    } else {
        if (y > 0) angle = Math.PI * 0.5;
        if (y < 0) angle = Math.PI * 1.5;
        if (y === 0) angle = 0;
    }

    return { angle: angle, ratio: ratio };
};

formulas.cartesianCoordinates = function (angle, ratio) {
    var x = ratio * Math.cos(angle);
    var y = ratio * Math.sin(angle);

    return { x: x, y: y };
};

module.exports = formulas;
