'use strict';

// Submodules
var Board = require('hexboard');
var canv = require('./canvas');
var configuration = require('./configuration');
var transformations = require('./transformations');
var animator = require('./animator');
var hoverCell = require('./hover-cell')


// local variables
var canvas = canv.canvas, context = canv.context;
var board, conf;


conf = Object.create(configuration);
conf.horizontalOffset = 100;
conf.verticalOffset   = 100;

init();


function init() {
    board = new Board(conf);
    board.savePoints();
    // rotateBoard();

    bindBoardEvents();
    window.addEventListener('resize', canvasResize);
    canvas.addEventListener('click', canvasClick);
    canvas.addEventListener('mousemove', canvasHover);

    canvasResize();

    window.requestAnimationFrame(draw);
}


function rotateBoard() {
    var angle = 2;
    var limits = transformations.getLimits(board);
    var center = limits.center;

    function rotate() {
        board.loadPoints();
        transformations.rotate2d(board, transformations.degreesToRadians(angle), center);
        transformations.rotate2d(board, transformations.degreesToRadians(70), center, ['z', 'y']);
        transformations.perspective(board, limits);
        board.calculateBoundaries();
        angle += 0.5;
    }

    rotate();
    setInterval(rotate, 20);
}

var tm = 0;

function draw(timestamp) {
    clean();
    animator.forward(timestamp);
    board.draw(context, { debug: false, drawCenter: true });
    window.requestAnimationFrame(draw);
}

function clean() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function bindBoardEvents() {
    board.onDrawCell = onDrawCell;
}


function onDrawCell(cell, board, context) {
    var point, p, first;

    if (!cell.selected) return;

    var distance = board.conf.distance;

    context.fillStyle = board.conf.hilightColor;
    context.beginPath();

    for (p in cell.points) {
        point = cell.points[p];

        if (!first) {
            first = point;
            context.moveTo(
                point.x * distance + board.conf.horizontalOffset,
                point.y * distance + board.conf.verticalOffset
            );
        } else {
            context.lineTo(
                point.x * distance + board.conf.horizontalOffset,
                point.y * distance + board.conf.verticalOffset
            );
        }
    }

    context.lineTo(
        first.x * distance + board.conf.horizontalOffset,
        first.y * distance + board.conf.verticalOffset
    );

    context.fill();
}


function canvasResize() {
    var width = window.innerWidth;
    var height = window.innerHeight;

    canvas.width = width;
    canvas.height = height;
}

function canvasClick(event) {
    board.selectCellFromPoint({ x: event.offsetX, y: event.offsetY }, board.selectCell);
}


function canvasHover(event) {
    // select the cell at point
    var cell = board.selectCellFromPoint({ x: event.offsetX, y: event.offsetY });
    if (!cell) return;

    // if it's the already selected cell return
    if (board.hoveredCell && board.hoveredCell.hoverOver == cell.name) return;

    if (!board.hoveredCell) {
        board.hoveredCell = hoverCell.create(cell);
        return;
    }

    board.hoveredCell.hoverOver = cell.name;

    animator.move({
        move: board.hoveredCell,
        from: board.hoveredCell,
        to: cell,
        time: 0.13
    });

}

function zoomCanvas() {
    console.log('zooming');
}
