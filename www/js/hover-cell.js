'use strict';

var Board = require('hexboard');
var Cell = Board.Cell;
var Point = Board.Point;
var Line = Board.Line;

var hoverCell = {};

hoverCell.create = function (cell) {
    var points = {};
    var hover = new Cell(cell);

    hover.name = 'hover';
    hover.hoverOver = cell.name;

    cell.points.forEach(function (p, n) {
        var point = new Point(p);
        points[p.name] = point;
        point.name = 'hoverpoint' + n;

        hover.points.push(point);
    });

    cell.lines.forEach(function (l, n) {
        var line = new Line(l);
        line.pointA = points[l.pointA.name];
        line.pointB = points[l.pointB.name];

        hover.lines.push(line);
    });

    return hover;
};

module.exports = hoverCell;
