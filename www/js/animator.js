'use strict';

var animator = {};

animator.queu = [];
animator.lastTimeStamp;

animator.move = function (p) {
    var element = p.move;
    var start = p.from;
    var end = p.to;
    var seconds = p.time;

    if (element.constructor.name == 'Cell') {
        animator.queu.push({
            objectToMove: element,
            startPoint: { x: start.x, y: start.y },
            endPoint: end,
            currentTime: 0,
            totalTime: seconds
        });

        element.points.forEach(function (point, n) {
            animator.queu.push({
                objectToMove: point,
                startPoint: { x: point.x, y: point.y },
                endPoint: end.points[n],
                currentTime: 0,
                totalTime: seconds
            });
        });
    }
};

animator.forward = function (timestamp) {
    if (!animator.lastTimeStamp) {
        animator.lastTimeStamp = timestamp;
        return;
    }

    if (timestamp < animator.lastTimeStamp) {
        animator.lastTimeStamp = timestamp;
        return;
    }

    var delta = timestamp - animator.lastTimeStamp;

    animator.lastTimeStamp = timestamp;
    animator.processQueue(delta / 1000);
};

animator.processQueue = function (delta) {

    animator.queu = animator.queu.map(function (animation) {
        var objectToMove = animation.objectToMove;
        var startPoint = animation.startPoint;
        var endPoint = animation.endPoint;
        var currentTime = animation.currentTime;
        var totalTime = animation.totalTime;

        animation.currentTime += delta;

        var distanceX = endPoint.x - startPoint.x;
        var distanceY = endPoint.y - startPoint.y;

        var movementPercentage = currentTime / animation.totalTime;

        var targetX = startPoint.x + distanceX * movementPercentage;
        var targetY = startPoint.y + distanceY * movementPercentage;

        if (animation.currentTime < animation.totalTime) {
            animation.objectToMove.x = targetX;
            animation.objectToMove.y = targetY;
        } else {
            animation.objectToMove.x = endPoint.x;
            animation.objectToMove.y = endPoint.y;
        }

        return animation;
    });

    animator.queu = animator.queu.filter(function (animation) {
        return (animation.currentTime < animation.totalTime);
    });
}

module.exports = animator;
