'use strict';

var canvas = document.getElementById('canvas');

module.exports = {
    canvas: canvas,
    context: canvas.getContext('2d')
};
