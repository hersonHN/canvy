
var gulp = require('gulp');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var browserify = require('gulp-browserify');

gulp.task('scripts', function() {
    return gulp.src('./www/js/main.js')
        .pipe(
            browserify()
            .on('error', function (err) {
            
                console.log(err.toString());

                this.emit('end');
            })
        )
        .pipe(gulp.dest('./www/build'))
});

gulp.task('sass', function () {
    return gulp.src('./www/sass/*.scss')
        .pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
        .pipe(gulp.dest('./www/css'));
});

gulp.task('watch', function() {
    watch('./www/js/*.js', function() {
        gulp.start('scripts');
    });

    watch('./www/sass/*.scss', function() {
        gulp.start('sass');
    });
});

gulp.task('default', ['sass', 'scripts', 'watch']);
